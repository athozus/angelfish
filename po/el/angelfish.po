# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-angelfish package.
#
# Stelios <sstavra@gmail.com>, 2019, 2020, 2021, 2023.
# George Stefanakis <george.stefanakis@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-angelfish\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-07-19 00:39+0000\n"
"PO-Revision-Date: 2023-07-24 13:16+0300\n"
"Last-Translator: George Stefanakis <george.stefanakis@gmail.com>\n"
"Language-Team: Greek <kde-i18n-doc@kde.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Stelios, Γιώργος Στεφανάκης"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "sstavra@gmail.com"

#: angelfish-webapp/main.cpp:71
#, kde-format
msgid "desktop file to open"
msgstr "αρχείο επιφάνειας εργασίας για άνοιγμα"

#: angelfish-webapp/main.cpp:98
#, kde-format
msgid "Angelfish Web App runtime"
msgstr "Εκτέλεση εφαρμογής ιστού Angelfish"

#: angelfish-webapp/main.cpp:100
#, kde-format
msgid "Copyright 2020 Angelfish developers"
msgstr "Copyright 2020 οι προγραμματιστές του Angelfish"

#: angelfish-webapp/main.cpp:102
#, kde-format
msgid "Marco Martin"
msgstr "Marco Martin"

#: lib/angelfishwebprofile.cpp:63
#, kde-format
msgid "Download finished"
msgstr "Η λήψη ολοκληρώθηκε"

#: lib/angelfishwebprofile.cpp:67
#, kde-format
msgid "Download failed"
msgstr "Αποτυχία λήψης"

#: lib/angelfishwebprofile.cpp:88 src/contents/ui/NewTabQuestion.qml:19
#, kde-format
msgid "Open"
msgstr "Άνοιγμα"

#: lib/contents/ui/AuthSheet.qml:17
#, kde-format
msgid "Authentication required"
msgstr "Απαιτείται ταυτοποίηση"

#: lib/contents/ui/AuthSheet.qml:30
#, kde-format
msgid "Username"
msgstr "Όνομα χρήστη"

#: lib/contents/ui/AuthSheet.qml:37
#, kde-format
msgid "Password"
msgstr "Κωδικός πρόσβασης"

#: lib/contents/ui/DownloadQuestion.qml:15
#, kde-format
msgid "Do you want to download this file?"
msgstr "Θέλετε να κάνετε λήψη αυτού του αρχείου;"

#: lib/contents/ui/DownloadQuestion.qml:23
#: src/contents/ui/AdblockFilterDownloadQuestion.qml:30
#, kde-format
msgid "Download"
msgstr "Λήψη"

#: lib/contents/ui/DownloadQuestion.qml:31 lib/contents/ui/PrintPreview.qml:163
#: src/contents/ui/Downloads.qml:92
#, kde-format
msgid "Cancel"
msgstr "Ακύρωση"

#: lib/contents/ui/ErrorHandler.qml:47
#, kde-format
msgid "Error loading the page"
msgstr "Σφάλμα στη φόρτωση της σελίδας"

#: lib/contents/ui/ErrorHandler.qml:58
#, kde-format
msgid "Retry"
msgstr "Νέα προσπάθεια"

#: lib/contents/ui/ErrorHandler.qml:71
#, kde-format
msgid ""
"Do you wish to continue?\n"
"\n"
" If you wish so, you may continue with an unverified certificate.\n"
" Accepting an unverified certificate means\n"
" you may not be connected with the host you tried to connect to.\n"
" Do you wish to override the security check and continue?"
msgstr ""
"Θέλετε να συνεχίσετε;\n"
"\n"
" Αν ναι, μπορείτε να συνεχίσετε με ένα μη επιβεβαιωμένο πιστοποιητικό.\n"
" Η αποδοχή ενός μη επιβεβαιωμένου πιστοποιητικού σημαίνει\n"
" ίσως να μη μπορείτε να συνδεθείτε με τον υπολογιστή που θέλετε.\n"
" Θέλετε να παρακάμψετε τον έλεγχο ασφαλείας και να συνεχίσετε;"

#: lib/contents/ui/ErrorHandler.qml:79
#, kde-format
msgid "Yes"
msgstr "Ναι"

#: lib/contents/ui/ErrorHandler.qml:88
#, kde-format
msgid "No"
msgstr "Όχι"

#: lib/contents/ui/JavaScriptDialogSheet.qml:49
#, kde-format
msgid "This page says"
msgstr "Αυτή η σελίδα λέει"

#: lib/contents/ui/JavaScriptDialogSheet.qml:64
#, kde-format
msgid ""
"The website asks for confirmation that you want to leave. Unsaved "
"information might not be saved."
msgstr ""
"Ο ιστότοπος ζητάει να επιβεβαιώσετε ότι θέλετε να αποχωρήσετε. Μη "
"αποθηκευμένες πληροφορίες ίσως να μην αποθηκευτούν."

#: lib/contents/ui/JavaScriptDialogSheet.qml:79
#, kde-format
msgid "Leave page"
msgstr "Αποχώρηση από σελίδα"

#: lib/contents/ui/JavaScriptDialogSheet.qml:87
#, kde-format
msgid "Submit"
msgstr "Υποβολή"

#: lib/contents/ui/PermissionQuestion.qml:19
#, kde-format
msgid "Do you want to allow the website to access the geo location?"
msgstr ""
"Θέλετε να επιτρέψετε στον ιστότοπο να έχει πρόσβαση στη γεωγραφική θέση;"

#: lib/contents/ui/PermissionQuestion.qml:22
#, kde-format
msgid "Do you want to allow the website to access the microphone?"
msgstr "Θέλετε να επιτρέψετε στον ιστότοπο να έχει πρόσβαση στο μικρόφωνο;"

#: lib/contents/ui/PermissionQuestion.qml:25
#, kde-format
msgid "Do you want to allow the website to access the camera?"
msgstr "Θέλετε να επιτρέψετε στον ιστότοπο να έχει πρόσβαση στην κάμερα;"

#: lib/contents/ui/PermissionQuestion.qml:28
#, kde-format
msgid ""
"Do you want to allow the website to access the camera and the microphone?"
msgstr ""
"Θέλετε να επιτρέψετε στον ιστότοπο να έχει πρόσβαση στην κάμερα και στο "
"μικρόφωνο;"

#: lib/contents/ui/PermissionQuestion.qml:31
#, kde-format
msgid "Do you want to allow the website to share your screen?"
msgstr "Θέλετε να επιτρέψετε στον ιστότοπο να έχει πρόσβαση στην οθόνη σας;"

#: lib/contents/ui/PermissionQuestion.qml:34
#, kde-format
msgid "Do you want to allow the website to share the sound output?"
msgstr "Θέλετε να επιτρέψετε στον ιστότοπο να έχει πρόσβαση στα ηχεία σας;"

#: lib/contents/ui/PermissionQuestion.qml:37
#, kde-format
msgid "Do you want to allow the website to send you notifications?"
msgstr "Θέλετε να επιτρέψετε στον ιστότοπο να σας στέλνει ειδοποιήσεις;"

#: lib/contents/ui/PermissionQuestion.qml:46
#, kde-format
msgid "Accept"
msgstr "Αποδοχή"

#: lib/contents/ui/PermissionQuestion.qml:56
#, kde-format
msgid "Decline"
msgstr "Απόρριψη"

#: lib/contents/ui/PrintPreview.qml:24 src/contents/ui/desktop/desktop.qml:296
#, kde-format
msgid "Print"
msgstr "Εκτύπωση"

#: lib/contents/ui/PrintPreview.qml:64
#, kde-format
msgid "Destination"
msgstr "Προορισμός"

#: lib/contents/ui/PrintPreview.qml:68
#, kde-format
msgid "Save to PDF"
msgstr "Αποθήκευση σε PDF"

#: lib/contents/ui/PrintPreview.qml:74
#, kde-format
msgid "Orientation"
msgstr "Προσανατολισμός"

#: lib/contents/ui/PrintPreview.qml:99
#, kde-format
msgid "Paper size"
msgstr "Μέγεθος χαρτιού"

#: lib/contents/ui/PrintPreview.qml:145
#, kde-format
msgid "Options"
msgstr "Επιλογές"

#: lib/contents/ui/PrintPreview.qml:149
#, kde-format
msgid "Print backgrounds"
msgstr "Εκτύπωση φόντων"

#: lib/contents/ui/PrintPreview.qml:168
#, kde-format
msgid "Save"
msgstr "Αποθήκευση"

#: lib/contents/ui/WebView.qml:243
#, kde-format
msgid "Website was opened in a new tab"
msgstr "Ο ιστότοπος άνοιξε σε νέα καρτέλα"

#: lib/contents/ui/WebView.qml:261
#, kde-format
msgid "Entered Full Screen Mode"
msgstr "Είσοδος σε λειτουργία Πλήρους Οθόνης"

#: lib/contents/ui/WebView.qml:262
#, kde-format
msgid "Exit Full Screen (Esc)"
msgstr "Έξοδος από πλήρη οθόνη (Esc)"

#: lib/contents/ui/WebView.qml:395
#, kde-format
msgid "Play"
msgstr "Αναπαραγωγή"

#: lib/contents/ui/WebView.qml:396 src/contents/ui/Downloads.qml:105
#, kde-format
msgid "Pause"
msgstr "Παύση"

#: lib/contents/ui/WebView.qml:403
#, kde-format
msgid "Unmute"
msgstr "Επαναφορά ήχου"

#: lib/contents/ui/WebView.qml:404
#, kde-format
msgid "Mute"
msgstr "Σίγαση"

#: lib/contents/ui/WebView.qml:414
#, kde-format
msgid "Speed"
msgstr "Ταχύτητα"

#: lib/contents/ui/WebView.qml:437
#, kde-format
msgid "Loop"
msgstr "Επανάληψη"

#: lib/contents/ui/WebView.qml:444
#, kde-format
msgid "Exit fullscreen"
msgstr "Έξοδος πλήρους οθόνης"

#: lib/contents/ui/WebView.qml:444
#, kde-format
msgid "Fullscreen"
msgstr "Πλήρης οθόνη"

#: lib/contents/ui/WebView.qml:457
#, kde-format
msgid "Hide controls"
msgstr "Απόκρυψη χειριστηριών"

#: lib/contents/ui/WebView.qml:458
#, kde-format
msgid "Show controls"
msgstr "Εμφάνιση χειριστηριών"

#: lib/contents/ui/WebView.qml:466
#, kde-format
msgid "Open video"
msgstr "Άνοιγμα βίντεο"

#: lib/contents/ui/WebView.qml:466
#, kde-format
msgid "Open audio"
msgstr "Άνοιγμα ήχου"

#: lib/contents/ui/WebView.qml:467
#, kde-format
msgid "Open video in new Tab"
msgstr "Άνοιγμα βίντεο σε νέα καρτέλα"

#: lib/contents/ui/WebView.qml:467
#, kde-format
msgid "Open audio in new Tab"
msgstr "Άνοιγμα ήχου σε νέα καρτέλα"

#: lib/contents/ui/WebView.qml:479
#, kde-format
msgid "Save video"
msgstr "Αποθήκευση βίντεο"

#: lib/contents/ui/WebView.qml:485
#, kde-format
msgid "Copy video Link"
msgstr "Αντιγραφή συνδέσμου βίντεο"

#: lib/contents/ui/WebView.qml:491
#, kde-format
msgid "Open image"
msgstr "Άνοιγμα εικόνας"

#: lib/contents/ui/WebView.qml:491
#, kde-format
msgid "Open image in new Tab"
msgstr "Άνοιγμα εικόνας σε νέα καρτέλα"

#: lib/contents/ui/WebView.qml:503
#, kde-format
msgid "Save image"
msgstr "Αποθήκευση εικόνας"

#: lib/contents/ui/WebView.qml:509
#, kde-format
msgid "Copy image"
msgstr "Αντιγραφή εικόνας"

#: lib/contents/ui/WebView.qml:515
#, kde-format
msgid "Copy image link"
msgstr "Αντιγραφή συνδέσμου εικόνας"

#: lib/contents/ui/WebView.qml:521
#, kde-format
msgid "Open link"
msgstr "Άνοιγμα συνδέσμου"

#: lib/contents/ui/WebView.qml:521
#, kde-format
msgid "Open link in new Tab"
msgstr "Άνοιγμα συνδέσμου σε νέα καρτέλα"

#: lib/contents/ui/WebView.qml:533
#, kde-format
msgid "Bookmark link"
msgstr "Προσθήκη συνδέσμου στους Σελιδοδείκτες"

#: lib/contents/ui/WebView.qml:545
#, kde-format
msgid "Save link"
msgstr "Αποθήκευση συνδέσμου"

#: lib/contents/ui/WebView.qml:551
#, kde-format
msgid "Copy link"
msgstr "Αντιγραφή συνδέσμου"

#: lib/contents/ui/WebView.qml:558
#, kde-format
msgid "Copy"
msgstr "Αντιγραφή"

#: lib/contents/ui/WebView.qml:564
#, kde-format
msgid "Cut"
msgstr "Αποκοπή"

#: lib/contents/ui/WebView.qml:570
#, kde-format
msgid "Paste"
msgstr "Επικόλληση"

#: lib/contents/ui/WebView.qml:591 src/contents/ui/desktop/desktop.qml:345
#: src/contents/ui/mobile.qml:297
#, kde-format
msgid "Share page"
msgstr "Διαμοιρασμός σελίδας"

#: lib/contents/ui/WebView.qml:603
#, kde-format
msgid "View page source"
msgstr "Προβολή κώδικα σελίδας"

#: src/contents/ui/AdblockFilterDownloadQuestion.qml:14
#, kde-format
msgid ""
"The ad blocker is missing its filter lists, do you want to download them now?"
msgstr ""
"Λείπουν από το απαγορευτικό διαφημίσεων οι λίστες των φίλτρων, θέλετε να "
"κάνετε λήψη τώρα;"

#: src/contents/ui/AdblockFilterDownloadQuestion.qml:35
#, kde-format
msgid "Downloading..."
msgstr "Λήψη..."

#: src/contents/ui/Bookmarks.qml:12 src/contents/ui/desktop/desktop.qml:274
#: src/contents/ui/mobile.qml:81
#, kde-format
msgid "Bookmarks"
msgstr "Σελιδοδείκτες"

#: src/contents/ui/desktop/BookmarksPage.qml:30
#, kde-format
msgid "Search bookmarks…"
msgstr "Αναζήτηση στους σελιδοδείκτες…"

#: src/contents/ui/desktop/BookmarksPage.qml:77
#, kde-format
msgid "No bookmarks yet"
msgstr "Κανένας σελιδοδείκτης ακόμη"

#: src/contents/ui/desktop/desktop.qml:125
#, kde-format
msgid "Search or enter URL…"
msgstr "Αναζήτηση ή εισαγωγή URL..."

#: src/contents/ui/desktop/desktop.qml:249 src/contents/ui/desktop/Tabs.qml:355
#: src/contents/ui/Navigation.qml:437
#, kde-format
msgid "New Tab"
msgstr "Νέα καρτέλα"

#: src/contents/ui/desktop/desktop.qml:256 src/contents/ui/mobile.qml:73
#, kde-format
msgid "Leave private mode"
msgstr "Να παραμείνει σε ιδιωτική λειτουργία"

#: src/contents/ui/desktop/desktop.qml:256 src/contents/ui/mobile.qml:73
#, kde-format
msgid "Private mode"
msgstr "Ιδιωτική λειτουργία"

#: src/contents/ui/desktop/desktop.qml:265 src/contents/ui/History.qml:12
#: src/contents/ui/mobile.qml:89
#, kde-format
msgid "History"
msgstr "Ιστορικό"

#: src/contents/ui/desktop/desktop.qml:283 src/contents/ui/Downloads.qml:16
#: src/contents/ui/mobile.qml:93
#, kde-format
msgid "Downloads"
msgstr "Λήψεις"

#: src/contents/ui/desktop/desktop.qml:302
#, kde-format
msgid "Full Screen"
msgstr "Πλήρης οθόνη"

#: src/contents/ui/desktop/desktop.qml:319
#, kde-format
msgid "Hide developer tools"
msgstr "Απόκρυψη εργαλείων προγραμματιστή"

#: src/contents/ui/desktop/desktop.qml:320 src/contents/ui/mobile.qml:393
#, kde-format
msgid "Show developer tools"
msgstr "Εμφάνιση εργαλείων προγραμματιστή"

#: src/contents/ui/desktop/desktop.qml:329 src/contents/ui/mobile.qml:293
#, kde-format
msgid "Find in page"
msgstr "Εύρεση σε σελίδα"

#: src/contents/ui/desktop/desktop.qml:338
#, kde-format
msgctxt "@action:inmenu"
msgid "Reader Mode"
msgstr "Λειτουργία αναγνώστη"

#: src/contents/ui/desktop/desktop.qml:355
#, kde-format
msgid "Add to application launcher"
msgstr "Προσθήκη στον εκτελεστή εφαρμογών"

#: src/contents/ui/desktop/desktop.qml:374 src/contents/ui/mobile.qml:101
#, kde-format
msgid "Settings"
msgstr "Ρυθμίσεις"

#: src/contents/ui/desktop/HistoryPage.qml:30
#, kde-format
msgid "Search history…"
msgstr "Αναζήτηση στο ιστορικό…"

#: src/contents/ui/desktop/HistoryPage.qml:46
#, kde-format
msgid "Clear all history"
msgstr "Καθαρισμός όλου του ιστορικού"

#: src/contents/ui/desktop/HistoryPage.qml:83
#, kde-format
msgid "Not history yet"
msgstr "Κανένα ιστορικό ακόμη"

#: src/contents/ui/desktop/Tabs.qml:232 src/contents/ui/Tabs.qml:275
#, kde-format
msgid "Reader Mode: %1"
msgstr "Λειτουργία αναγνώστη: %1"

#: src/contents/ui/desktop/Tabs.qml:261 src/contents/ui/Tabs.qml:306
#, kde-format
msgid "Close tab"
msgstr "Κλείσιμο καρτέλας"

#: src/contents/ui/desktop/Tabs.qml:291
#, kde-format
msgid "Open a new tab"
msgstr "Άνοιγμα νέας καρτέλας"

#: src/contents/ui/desktop/Tabs.qml:324
#, kde-format
msgid "List all tabs"
msgstr "Προβολή όλων των καρτελών"

#: src/contents/ui/desktop/Tabs.qml:364
#, kde-format
msgid "Reload Tab"
msgstr "Επαναφόρτωση Καρτέλας"

#: src/contents/ui/desktop/Tabs.qml:372
#, kde-format
msgid "Duplicate Tab"
msgstr "Διπλότυπη Καρτέλα"

#: src/contents/ui/desktop/Tabs.qml:380
#, kde-format
msgid "Bookmark Tab"
msgstr "Προσθήκη καρτέλας στους Σελιδοδείκτες"

#: src/contents/ui/desktop/Tabs.qml:396
#, kde-format
msgid "Close Tab"
msgstr "Κλείσιμο καρτέλας"

#: src/contents/ui/Downloads.qml:28
#, kde-format
msgid "No running downloads"
msgstr "Δεν εκτελούνται λήψεις"

#: src/contents/ui/Downloads.qml:77
#, kde-format
msgctxt "download state"
msgid "Starting…"
msgstr "Έναρξη…"

#: src/contents/ui/Downloads.qml:79
#, kde-format
msgid "Completed"
msgstr "Ολοκληρώθηκε"

#: src/contents/ui/Downloads.qml:81
#, kde-format
msgid "Cancelled"
msgstr "Ακυρώθηκε"

#: src/contents/ui/Downloads.qml:83
#, kde-format
msgctxt "download state"
msgid "Interrupted"
msgstr "Διακόπηκε"

#: src/contents/ui/Downloads.qml:85
#, kde-format
msgctxt "download state"
msgid "In progress"
msgstr "Σε εξέλιξη"

#: src/contents/ui/Downloads.qml:117
#, kde-format
msgid "Continue"
msgstr "Συνέχεια"

#: src/contents/ui/FindInPageBar.qml:45
#, kde-format
msgid "Search..."
msgstr "Αναζήτηση..."

#: src/contents/ui/mobile.qml:20
#, kde-format
msgid "Angelfish Web Browser"
msgstr "Περιηγητής ιστού Angelfish"

#: src/contents/ui/mobile.qml:66
#, kde-format
msgid "Tabs"
msgstr "Καρτέλες"

#: src/contents/ui/mobile.qml:308
#, kde-format
msgid "Add to homescreen"
msgstr "Προσθήκη στην προσωπική οθόνη"

#: src/contents/ui/mobile.qml:318
#, kde-format
msgid "Open in app"
msgstr "Άνοιγμα σε εφαρμογή"

#: src/contents/ui/mobile.qml:326
#, kde-format
msgid "Go previous"
msgstr "Μετάβαση στην προηγούμενη"

#: src/contents/ui/mobile.qml:334 src/settings/SettingsNavigationBarPage.qml:59
#, kde-format
msgid "Go forward"
msgstr "Μετάβαση στην επόμενη"

#: src/contents/ui/mobile.qml:341
#, kde-format
msgid "Stop loading"
msgstr "Διακοπή φόρτωσης"

#: src/contents/ui/mobile.qml:341
#, kde-format
msgid "Refresh"
msgstr "Ανανέωση"

#: src/contents/ui/mobile.qml:351
#, kde-format
msgid "Bookmarked"
msgstr "Στους σελιδοδείκτες"

#: src/contents/ui/mobile.qml:351
#, kde-format
msgid "Bookmark"
msgstr "Σελιδοδείκτης"

#: src/contents/ui/mobile.qml:367
#, kde-format
msgid "Show desktop site"
msgstr "Εμφάνιση τόπου επιφάνειας εργασίας"

#: src/contents/ui/mobile.qml:376
#, kde-format
msgid "Reader Mode"
msgstr "Λειτουργία αναγνώστη"

#: src/contents/ui/mobile.qml:384
#, kde-format
msgid "Hide navigation bar"
msgstr "Απόκρυψη γραμμής πλοήγησης"

#: src/contents/ui/Navigation.qml:410
#, kde-format
msgid "Done"
msgstr ""

#: src/contents/ui/NewTabQuestion.qml:11
#, kde-format
msgid ""
"Site wants to open a new tab: \n"
"%1"
msgstr ""
"Ο ιστότοπος θέλει να ανοίξει μια νέα καρτέλα: \n"
"%1"

#: src/contents/ui/ShareSheet.qml:18
#, kde-format
msgid "Share page to"
msgstr "Διαμοιρασμός σελίδας με"

#: src/main.cpp:82
#, kde-format
msgid "URL to open"
msgstr "URL για άνοιγμα"

#: src/settings/AngelfishConfigurationView.qml:14
#: src/settings/SettingsGeneral.qml:18
#, kde-format
msgid "General"
msgstr "Γενικά"

#: src/settings/AngelfishConfigurationView.qml:20
#, kde-format
msgid "Ad Block"
msgstr "Ad Block"

#: src/settings/AngelfishConfigurationView.qml:26
#: src/settings/SettingsWebApps.qml:16
#, kde-format
msgid "Web Apps"
msgstr "Εφαρμογές ιστού"

#: src/settings/AngelfishConfigurationView.qml:32
#: src/settings/SettingsSearchEnginePage.qml:17
#: src/settings/SettingsSearchEnginePage.qml:112
#, kde-format
msgid "Search Engine"
msgstr "Μηχανή αναζήτησης"

#: src/settings/AngelfishConfigurationView.qml:38
#: src/settings/DesktopHomeSettingsPage.qml:15
#, kde-format
msgid "Toolbars"
msgstr "Γραμμές εργαλείων"

#: src/settings/DesktopHomeSettingsPage.qml:24
#, kde-format
msgid "Show home button:"
msgstr "Να εμφανίζεται το κουμπί της αρχικής σελίδας:"

#: src/settings/DesktopHomeSettingsPage.qml:25
#, kde-format
msgid "The home button will be shown next to the reload button in the toolbar."
msgstr ""
"Στη γραμμή εργαλείων το κουμπί Αρχική θα εμφανίζεται δίπλα στο κουμπί "
"ανανέωση."

#: src/settings/DesktopHomeSettingsPage.qml:35
#, kde-format
msgid "Homepage:"
msgstr "Αρχική:"

#: src/settings/DesktopHomeSettingsPage.qml:58
#, kde-format
msgid "New tabs:"
msgstr "Νέες καρτέλες:"

#: src/settings/DesktopHomeSettingsPage.qml:81
#, kde-format
msgid "Always show the tab bar"
msgstr "Να εμφανίζεται πάντα η γραμμή καρτελών"

#: src/settings/DesktopHomeSettingsPage.qml:82
#, kde-format
msgid "The tab bar will be displayed even if there is only one tab open"
msgstr ""
"Η γραμμή καρτελών θα εμφανίζεται πάντα, ακόμα και αν είναι ανοικτή μόνο μια "
"καρτέλα"

#: src/settings/SettingsAdblock.qml:16
#, kde-format
msgid "Adblock settings"
msgstr "Ρυθμίσεις απαγορευτικού διαφημίσεων"

#: src/settings/SettingsAdblock.qml:23
#, fuzzy, kde-format
#| msgid "add Filterlist"
msgctxt "@action:intoolbar"
msgid "Add Filterlist"
msgstr "Προσθήκη λίστας φίλτρων"

#: src/settings/SettingsAdblock.qml:27
#, kde-format
msgid "Update lists"
msgstr "Ενημέρωση λιστών"

#: src/settings/SettingsAdblock.qml:49
#, kde-format
msgid "The adblock functionality isn't included in this build."
msgstr ""
"Η λειτουργία του απαγορευτικού διαφημίσεων δεν περιλαμβάνεται σε αυτήν την "
"έκδοση."

#: src/settings/SettingsAdblock.qml:100
#, kde-format
msgid "Remove this filter list"
msgstr "Αφαίρεση αυτής της λίστας φίλτρων"

#: src/settings/SettingsAdblock.qml:112
#, fuzzy, kde-format
#| msgid "add Filterlist"
msgid "Add Filterlist"
msgstr "Προσθήκη λίστας φίλτρων"

#: src/settings/SettingsAdblock.qml:124
#, kde-format
msgid "Add filterlist"
msgstr "Προσθήκη λίστας φίλτρων"

#: src/settings/SettingsAdblock.qml:129
#, kde-format
msgid "Name"
msgstr "Όνομα"

#: src/settings/SettingsAdblock.qml:138
#, kde-format
msgid "Url"
msgstr "Url"

#: src/settings/SettingsGeneral.qml:27
#, kde-format
msgid "Enable JavaScript"
msgstr "Ενεργοποίηση JavaScript"

#: src/settings/SettingsGeneral.qml:28
#, kde-format
msgid "This may be required on certain websites for them to work."
msgstr "Αυτό ίσως απαιτείται σε ορισμένους ιστοτόπους για να λειτουργήσουν."

#: src/settings/SettingsGeneral.qml:37
#, kde-format
msgid "Load images"
msgstr "Φόρτωση εικόνων"

#: src/settings/SettingsGeneral.qml:38
#, kde-format
msgid "Whether to load images on websites."
msgstr "Αν θα φορτώνονται εικόνες σε ιστοτόπους."

#: src/settings/SettingsGeneral.qml:47
#, kde-format
msgid "Enable adblock"
msgstr "Ενεργοποίηση απαγορευτικού διαφημίσεων"

#: src/settings/SettingsGeneral.qml:48
#, kde-format
msgid "Attempts to prevent advertisements on websites from showing."
msgstr "Επιχειρεί να εμποδίσει την εμφάνιση διαφημίσεων σε ιστοτόπους."

#: src/settings/SettingsGeneral.qml:48
#, kde-format
msgid "AdBlock functionality was not included in this build."
msgstr ""
"Η λειτουργία του απαγορευτικού διαφημίσεων δεν περιλαμβάνεται σε αυτήν την "
"έκδοση."

#: src/settings/SettingsGeneral.qml:58
#, kde-format
msgid "Switch to new tab immediately"
msgstr "Άμεση εναλλαγή σε νέα καρτέλα"

#: src/settings/SettingsGeneral.qml:59
#, kde-format
msgid ""
"When you open a link, image or media in a new tab, switch to it immediately"
msgstr ""
"Με το άνοιγμα συνδέσμου, εικόνας ή πολυμέσου σε νέα καρτέλα να γίνεται άμεσα "
"εναλλαγή σε αυτήν."

#: src/settings/SettingsGeneral.qml:68
#, kde-format
msgid "Use Smooth Scrolling"
msgstr "Χρήση Ομαλής Κύλισης"

#: src/settings/SettingsGeneral.qml:69
#, kde-format
msgid ""
"Scrolling is smoother and will not stop suddenly when you stop scrolling. "
"Requires app restart to take effect."
msgstr ""
"Η κύλιση είναι ομαλότερη και δεν θα σταματήσει ξαφνικά, ακόμα και αν "
"σταματήσετε την κύλιση. Απαιτεί επαννεκίνηση της εφαρμογής για ενεργοποίηση."

#: src/settings/SettingsGeneral.qml:78
#, kde-format
msgid "Use dark color scheme"
msgstr "Χρήση σκοτεινού θέματος"

#: src/settings/SettingsGeneral.qml:79
#, kde-format
msgid ""
"Websites will have their color schemes set to dark. Requires app restart to "
"take effect."
msgstr ""
"Οι ιστοσελίδες θα έχουν το χρωματικό τους θέμα σε σκοτεινό.  Απαιτεί "
"επαννεκίνηση για να τεθεί σε λειτουργία."

#: src/settings/SettingsNavigationBarPage.qml:18
#, kde-format
msgid "Navigation bar"
msgstr "Γραμμή πλοήγησης"

#: src/settings/SettingsNavigationBarPage.qml:26
#, fuzzy, kde-format
#| msgid ""
#| "Choose the buttons enabled in navigation bar. Some of the buttons can be "
#| "hidden only in portrait orientation of the browser and are always shown "
#| "if  the browser is wider than its height.\n"
#| "\n"
#| " Note that if you disable the menu buttons, you will be able to access "
#| "the menus either by swiping from the left or right side or to a side "
#| "along the bottom of the window."
msgid ""
"Choose the buttons enabled in navigation bar. Some of the buttons can be "
"hidden only in portrait orientation of the browser and are always shown if "
"the browser is wider than its height.\n"
"\n"
" Note that if you disable the menu buttons, you will be able to access the "
"menus either by swiping from the left or right side or to a side along the "
"bottom of the window."
msgstr ""
"Επιλέξτε τα ενεργά κουμπιά στη γραμμή πλοήγησης. Μερικά κουμπιά μπορεί να "
"είναι κρυμμένα μόνο κατακόρυφα στον περιηγητή και εμφανίζονται πάντα αν ο "
"περιηγητής έχει πλάτος μεγαλύτερο από το ύψος του.\n"
"\n"
" Σημειώστε ότι αν απενεργοποιήσετε τα κουμπιά του μενού, θα έχετε πρόσβαση "
"σε αυτό ή χτυπώντας από την αριστερή ή τη δεξιά πλευρά ή σε μια πλευρά κατά "
"μήκος της βάσης του παραθύρου."

#: src/settings/SettingsNavigationBarPage.qml:35
#, kde-format
msgid "Main menu in portrait"
msgstr "Κύριο μενού στο πορτρέτο"

#: src/settings/SettingsNavigationBarPage.qml:41
#, kde-format
msgid "Tabs in portrait"
msgstr "Καρτέλες στο πορτρέτο"

#: src/settings/SettingsNavigationBarPage.qml:47
#, kde-format
msgid "Context menu in portrait"
msgstr "Μενού επιλογών στο πορτρέτο"

#: src/settings/SettingsNavigationBarPage.qml:53
#, kde-format
msgid "Go back"
msgstr "Μετάβαση πίσω"

#: src/settings/SettingsNavigationBarPage.qml:65
#, kde-format
msgid "Reload/Stop"
msgstr "Επαναφόρτωση/διακοπή"

#: src/settings/SettingsSearchEnginePage.qml:20
#, kde-format
msgid "Custom"
msgstr "Προσαρμοσμένο"

#: src/settings/SettingsSearchEnginePage.qml:117
#, kde-format
msgid "Base URL of your preferred search engine"
msgstr "URL βάσης της μηχανής αναζήτησης της προτίμησής σας"

#: src/settings/SettingsWebApps.qml:73
#, kde-format
msgid "Remove app"
msgstr "Διαγραφή εφαρμογής"

#: src/settings/SettingsWebApps.qml:85
#, kde-format
msgctxt "placeholder message"
msgid "No Web Apps installed"
msgstr ""

#~ msgid "Show navigation bar"
#~ msgstr "Εμφάνιση γραμμής πλοήγησης"

#~ msgid "Configure Angelfish"
#~ msgstr "Διαμόρφωση Angelfish"

#~ msgid "Add"
#~ msgstr "Προσθήκη"

#, fuzzy
#~| msgid "Private mode"
#~ msgid "Private Mode"
#~ msgstr "Ιδιωτική λειτουργία"

#~ msgid "Enabled"
#~ msgstr "Ενεργό"

#~ msgid "Disabled"
#~ msgstr "Απενεργοποιημένο"

#~ msgid "Search online for '%1'"
#~ msgstr "Αναζήτηση σε σύνδεση για το '%1'"

#~ msgid "Search online"
#~ msgstr "Αναζήτηση σε σύνδεση"

#~ msgid "Confirm"
#~ msgstr "Επιβεβαίωση"

#~ msgid "OK"
#~ msgstr "Εντάξει"

#~ msgid "Enable Adblock"
#~ msgstr "Ενεργοποίηση απαγορευτικού διαφημίσεων"

#, fuzzy
#~| msgid "Homepage"
#~ msgid "Home"
#~ msgstr "Αρχική"

#~ msgid "Adblock filter lists"
#~ msgstr "Λίστες φίλτρων απαγορευτικών διαφημίσεων"

#~ msgid "New"
#~ msgstr "Νέα"

#~ msgid "Choose the buttons enabled in navigation bar. "
#~ msgstr "Επιλέξτε τα ενεργά κουμπιά στη γραμμή πλοήγησης. "

#~ msgid "Website that should be loaded on startup"
#~ msgstr "Ιστότοπος που φορτώνεται στην εκκίνηση"

#~ msgid "Find..."
#~ msgstr "Εύρεση..."

#~ msgid "Highlight text on the current website"
#~ msgstr "Τονισμός κειμένου στον τρέχοντα ιστότοπο"

#~ msgid "Start without UI"
#~ msgstr "Εκκίνηση χωρίς περιβάλλον χρήστη"

#~ msgid "geo location"
#~ msgstr "γεωγραφική θέση"

#~ msgid "the microphone"
#~ msgstr "το μικρφωνο"

#~ msgid "the camera"
#~ msgstr "η κάμερα"

#~ msgid "camera and microphone"
#~ msgstr "κάμερα και μικρόφωνο"
